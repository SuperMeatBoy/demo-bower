import { compose } from "redux";
import {
  createStore,
  getDefaultMiddleware,
  applyMiddleware,
} from "@reduxjs/toolkit";
import { routerMiddleware } from "connected-react-router";
import createReducer from "./root-reducer.js";

const composeEnhancers =
  typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
    : compose;

export function configureStore(history) {
  const routeMiddleware = routerMiddleware(history);
  const middlewares = [...getDefaultMiddleware(), routeMiddleware];
  const store = createStore(
    createReducer(history),
    composeEnhancers(applyMiddleware(...middlewares))
  );

  return store;
}
