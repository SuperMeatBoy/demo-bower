import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import { reducer as rootSlices } from "./root-slices";

export default function createReducer(history) {
  const appReducer = combineReducers({
    router: connectRouter(history),
    root: rootSlices,
  });

  const rootReducer = (state, action) => {
    if (action.type === "auth/logout") {
      state = undefined;
    }
    return appReducer(state, action);
  };

  return rootReducer;
}
