import { createSlice } from "@reduxjs/toolkit";
import ROLES from "constants/roles";
import { color } from "components/variable";

export const rootSlice = createSlice({
  name: "root",
  initialState: {
    isLoading: false,
    locale: "en",
    role: ROLES.ADMIN,
    isMobileView: global.window.innerWidth < 768,
    isMobileMenuOpen: false,
    theme: {
      name: "light",
      primaryBg: color.grey_F6,
      secondaryBg: color.yellow,
      primaryText: color.blue_dark,
      secondaryText: color.yellow_light,
      error: color.red_cancel,
      success: color.green_light,
    },
    isHeaderFixed: false,
  },
  reducers: {
    setTheme: (state, { payload }) => {
      state.isLoading = false;
      state.theme = payload;
    },
    setLocale: (state, { payload }) => {
      state.isLoading = false;
      state.locale = payload;
    },
  },
});
export const { setTheme, setLocale } = rootSlice.actions;
export const { reducer } = rootSlice;
