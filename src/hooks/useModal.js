import React, { useState } from 'react'

const Modal = ({ children, modalProps }) => <div {...modalProps}>{children}</div>

export const useModal = () => {
  const [visible, setVisible] = useState(false)

  const showModal = () => setVisible(true)
  const hideModal = () => setVisible(false)

  return { Modal, showModal, hideModal, visible }
}
