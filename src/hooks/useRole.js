/* eslint-disable react-hooks/rules-of-hooks */
import { useMemo } from 'react'
import { useSelector } from 'react-redux'
import ROLES from 'constants/roles'
const {
  SUPER_ADMIN,
  ADMIN,
  CUSTOMER,
  USER,
} = ROLES

export const useRole = () => {
  const role = useSelector(({ user }) => user.role)
  const isSuperAdmin = useMemo(() => role === SUPER_ADMIN, [role])
  const isAdmin = useMemo(() => role === SUPER_ADMIN || role === ADMIN, [role])
  const isCustomer = useMemo(() => role === CUSTOMER, [role])
  const isUser = useMemo(() => role === USER, [role])

  return { role, isAdmin, isCustomer, isSuperAdmin, isUser }
}
