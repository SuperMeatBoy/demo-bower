import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import history from "utils/history";
import { configureStore } from "store/configureStore";
import "normalize.css";
import { GlobalStyle } from "./globalStyle";

import Router from "./router";
import LocaleTheme from "./localetheme";

const store = configureStore(history);

ReactDOM.render(
  /*<React.StrictMode>*/
  <Provider store={store}>
    <LocaleTheme>
      <GlobalStyle />
      <Router history={history} />
    </LocaleTheme>
  </Provider>,
  /*</React.StrictMode>*/ document.getElementById("root")
);
