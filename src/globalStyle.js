import styled, { createGlobalStyle, css } from "styled-components";
import { color } from "./components/variable";

export const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    outline: none;
    box-shadow: none;
  }
  html, body{
    height: 100%;
    font-size: 16px;
  }
  body{
    font-family: 'Open Sans', sans-serif;
    background-color: ${({ theme }) => theme.primaryBg};
  }
  #root{
    height: 100%;
  }
  a, svg{
    transition: all .3s;
  }
`;

export const Card = styled.div`
  padding: 10px;
  background-color: ${color.white};
  margin-bottom: 15px;
  border-radius: 8px;
  box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.01);

  ${(p) =>
    p.m0 &&
    css`
      margin: 0;
    `}
`;
