import styled from "styled-components";

export const HelpStyle = styled.div`
  display: flex;
  justify-content: center;
  cursor: pointer;
  svg {
    margin-right: 10px;
  }
`;
