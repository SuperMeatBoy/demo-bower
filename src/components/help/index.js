import React from "react";
import { HelpStyle } from "./style";
import Icon from "components/icon";

const Help = () => {
  return (
    <HelpStyle>
      <Icon component="support" />
      Help
    </HelpStyle>
  );
};

export default Help;
