import React from "react";
import { FooterStyle } from "./style";

const Footer = () => {
  return <FooterStyle>©2018 Created by Nico</FooterStyle>;
};
export default Footer;
