import styled from "styled-components";

export const LogoStyle = styled.div`
  display: block;
  img {
    display: block;
    max-width: 100%;
  }
`;
