import React from "react";
import { LogoStyle } from "./style";
import LogoImg from "static/logo.png";

const Logo = () => {
  return (
    <LogoStyle>
      <img src={LogoImg} alt="Logo" />
    </LogoStyle>
  );
};

export default Logo;
