export const color = {
  yellow: "#FFC42E",
  yellow_light: "#FFF5C8",
  blue_dark: "#051E28",
  blue_light: "#172C36",
  red_cancel: "#FC4848",
  green_light: "#26E89E",
  grey_F6: "#f6f6f6",

  white: "#fff",
};
