import styled from "styled-components";

export const Menu = styled.div`
  width: 100%;
  a {
    display: flex;
    align-items: center;
    padding: 10px 20px;
    text-decoration: none;
    color: ${({ theme }) => theme.primaryText};
    &:hover,
    &.active {
      background-color: ${({ theme }) => theme.secondaryBg};
    }
  }
  svg {
    margin-right: 10px;
    fill: ${({ theme }) => theme.primaryText};
  }
`;
