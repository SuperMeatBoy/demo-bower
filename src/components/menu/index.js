import React, { useState, useEffect } from "react";
import { Menu } from "./style";
import { NavLink, withRouter } from "react-router-dom";
import { menuData } from "services/menu";
import ROLE from "constants/roles";
import Icon from "components/icon";

const MenuLeft = ({ location: { pathname } }) => {
  const [collapsed, setCollapsed] = useState(false);
  const role = ROLE.SUPER_ADMIN;

  useEffect(() => {
    applySelectedKeys();
  }, [pathname, menuData]);

  const applySelectedKeys = () => {
    const flattenItems = (items, key) =>
      items.reduce((flattenedItems, item) => {
        flattenedItems.push(item);
        if (Array.isArray(item[key])) {
          return flattenedItems.concat(flattenItems(item[key], key));
        }
        return flattenedItems;
      }, []);
  };

  const generateMenuItems = () => {
    const generateItem = (item) => {
      const { key, title, url, icon, disabled, count } = item;
      if (item.category) {
        return <div key={Math.random()} title={item.title} />;
      }
      if (item.url) {
        return (
          <div key={key} disabled={disabled}>
            {item.target && (
              <a href={url} target={item.target} rel="noopener noreferrer">
                {!!icon && <Icon component={icon} />}
                <span>{title}</span>
                {!!count && <span>{count}</span>}
              </a>
            )}
            {!item.target && (
              <NavLink to={url}>
                {!!icon && <Icon component={icon} />}
                <span>{title}</span>
                {!!count && (
                  <span className="badge badge-success ml-2">{count}</span>
                )}
              </NavLink>
            )}
          </div>
        );
      }
      return (
        <div key={key} disabled={disabled}>
          <span>{title}</span>
          {count && <span className="badge badge-success ml-2">{count}</span>}
          {icon && <span className={`${icon} icon-collapsed-hidden`} />}
        </div>
      );
    };

    const generateSubmenu = (items) =>
      items.map((menuItem) => {
        if (menuItem.children) {
          const subMenuTitle = (
            <span key={menuItem.key}>
              <span>{menuItem.title}</span>
              {menuItem.count && (
                <span className="badge badge-success ml-2">
                  {menuItem.count}
                </span>
              )}
              {menuItem.icon && <span className={`${menuItem.icon}`} />}
            </span>
          );
          return (
            <div title={subMenuTitle} key={menuItem.key}>
              {generateSubmenu(menuItem.children)}
            </div>
          );
        }
        return generateItem(menuItem);
      });

    return menuData.map((menuItem) => {
      if (menuItem.roles && !menuItem.roles.includes(role)) {
        return null;
      }
      if (menuItem.children) {
        const subMenuTitle = (
          <span key={menuItem.key}>
            <span>{menuItem.title}</span>
            {menuItem.count && (
              <span className="badge badge-success ml-2">{menuItem.count}</span>
            )}
            {menuItem.icon && <span className={`${menuItem.icon}`} />}
          </span>
        );
        return (
          <div title={subMenuTitle} key={menuItem.key}>
            {generateSubmenu(menuItem.children)}
          </div>
        );
      }
      return generateItem(menuItem);
    });
  };

  return <Menu>{generateMenuItems()}</Menu>;
};

export default withRouter(MenuLeft);
