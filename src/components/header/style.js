import styled from "styled-components";

export const HeaderStyle = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
  position: sticky;
  top: 0;
  padding: 10px 15px 15px;
  margin: 0 -7px;
  & > div {
    padding: 0 7px;
    &:first-child {
      flex-grow: 1;
    }
  }
  .header-item {
    display: flex;
    min-height: 46px;
    align-items: center;
  }
`;
