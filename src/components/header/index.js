import React from "react";
import { HeaderStyle } from "./style";
import { Card } from "../../globalStyle";
import { color } from "../variable";
import { setTheme, setLocale } from "store/root-slices";
import { useDispatch } from "react-redux";

const light = {
  name: "light",
  primaryBg: color.grey_F6,
  secondaryBg: color.yellow,
  primaryText: color.blue_dark,
  secondaryText: color.yellow_light,
  error: color.red_cancel,
  success: color.green_light,
};
const dark = {
  name: "dark",
  primaryBg: color.blue_dark,
  secondaryBg: color.yellow,
  primaryText: color.blue_dark,
  secondaryText: color.yellow_light,
  error: color.red_cancel,
  success: color.green_light,
};

const Header = () => {
  const dispatch = useDispatch();
  const handleTheme = (e) => {
    const theme = e.currentTarget.value;
    dispatch(setTheme(theme === "light" ? light : dark));
  };
  const handleLocale = (e) => {
    const locale = e.currentTarget.value;
    dispatch(setLocale(locale));
  };

  return (
    <HeaderStyle>
      <div>
        <Card className="header-item" m0>
          Search
        </Card>
      </div>
      <div>
        <Card className="header-item" m0>
          <select onChange={handleTheme}>
            <option value="light" default>
              Light Theme
            </option>
            <option value="dark">Dark Theme</option>
          </select>
        </Card>
      </div>
      <div>
        <Card className="header-item" m0>
          <select onChange={handleLocale}>
            <option value="en" default>
              En
            </option>
            <option value="sv">Sv</option>
          </select>
        </Card>
      </div>
      <div>
        <Card className="header-item" m0>
          User Avatar
        </Card>
      </div>
    </HeaderStyle>
  );
};
export default Header;
