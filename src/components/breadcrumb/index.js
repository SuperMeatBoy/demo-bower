import React from "react";
import { BreadcrumbStyle } from "./style";
import { Card } from "globalStyle";

const Breadcrumb = () => {
  return (
    <Card>
      <BreadcrumbStyle>
        <div>Breadcrumb > Home</div>
      </BreadcrumbStyle>
    </Card>
  );
};

export default Breadcrumb;
