import styled from "styled-components";
import { color } from "../variable";

export const BreadcrumbStyle = styled.header`
  display: flex;
  align-items: center;
`;
