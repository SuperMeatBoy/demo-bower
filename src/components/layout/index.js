import React from "react";
import Menu from "components/menu";
import { LayoutStyle, Body, Content, SideLeft } from "./style";
import Header from "components/header";
import Breadcrumb from "components/breadcrumb";
import Footer from "../footer";
import Logo from "../logo";
import Help from "../help";

const Layout = ({ children }) => {
  return (
    <LayoutStyle>
      <SideLeft>
        <Logo />
        <Menu />
        <Help />
      </SideLeft>
      <Body>
        <Header />
        <Content>
          <Breadcrumb />
          {children}
        </Content>
        {/*<Footer />*/}
      </Body>
    </LayoutStyle>
  );
};

export default Layout;
