import styled from "styled-components";
import { color } from "../variable";

export const LayoutStyle = styled.div`
  display: flex;
  height: 100%;
`;
export const Body = styled.div`
  flex-grow: 1;
  max-width: calc(100% - 200px);
  position: relative;
  overflow-x: hidden;
  overflow-y: auto;
`;
export const Content = styled.div`
  padding: 0 15px;
`;
export const SideLeft = styled.div`
  flex: 0 0 200px;
  max-width: 200px;
  position: sticky;
  top: 0;
  z-index: 10;
  box-shadow: 2px 0 10px rgba(0, 0, 0, 0.1);
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-between;
  background-color: ${color.white};
  padding: 20px 0;
  border-radius: 0 8px 8px 0;
`;
