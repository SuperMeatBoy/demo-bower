const ENDPOINT = {
  authorize: '/sanctum/token',
  logout: '/sanctum/logout',
}

export default ENDPOINT
