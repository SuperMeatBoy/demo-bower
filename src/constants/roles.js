export const ROLES = {
  SUPER_ADMIN: "Super Admin",
  ADMIN: "Admin",
};

export default ROLES;
