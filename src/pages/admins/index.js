import React from "react";
import { Helmet } from "react-helmet";

import endpoints from "constants/endpoints";
import { useRole } from "hooks/useRole";
import Layout from "components/layout";
import { FormattedMessage } from "react-intl";
import { Card } from "../../globalStyle";

const Admins = () => {
  return (
    <Layout>
      <Helmet title="Admins" />
      <Card style={{ minHeight: "300px" }}>
        <div>Admins</div>
      </Card>
    </Layout>
  );
};

export default Admins;
