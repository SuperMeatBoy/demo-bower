import React from "react";
import { Helmet } from "react-helmet";

import endpoints from "constants/endpoints";
import { useRole } from "hooks/useRole";
import Layout from "components/layout";
import { FormattedMessage } from "react-intl";
import { Card } from "../../globalStyle";

const Support = () => {
  return (
    <Layout>
      <Helmet title="Support" />
      <Card style={{ minHeight: "300px" }}>
        <div>Support</div>
      </Card>
    </Layout>
  );
};

export default Support;
