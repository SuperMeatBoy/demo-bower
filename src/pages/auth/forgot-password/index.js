import React from 'react'
import { Helmet } from 'react-helmet'

const SystemForgotPassword = () => {
  return (
    <div>
      <Helmet title="Forgot Password" />
      <div>ForgotPassword</div>
    </div>
  )
}

export default SystemForgotPassword
