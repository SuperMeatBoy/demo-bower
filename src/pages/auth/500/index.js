import React from 'react'
import { Helmet } from 'react-helmet'

const System500 = () => {
  return (
    <div>
      <Helmet title="Page 500" />
      <div>Error500</div>
    </div>
  )
}

export default System500
