import React from 'react'
import { Helmet } from 'react-helmet'

const System404 = () => {
  return (
    <div>
      <Helmet title="Page 404" />
      <div>Error404</div>
    </div>
  )
}

export default System404
