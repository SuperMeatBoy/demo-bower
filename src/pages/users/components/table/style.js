import styled from "styled-components";
import { color } from "components/variable";

export const Table = styled.table`
  width: 100%;
  border-collapse: collapse;
  tr {
    &:hover {
      background-color: ${color.grey_F6};
    }
  }
  th,
  td {
    text-align: center;
    padding: 7px;
  }
  td {
    cursor: pointer;
  }
`;
