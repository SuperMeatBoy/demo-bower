import React from "react";
import { Card } from "globalStyle";
import Layout from "components/layout";
import { useSelector } from "react-redux";
import { users } from "pages/users/data";
import { Info } from "./style";

const User = () => {
  const { location } = useSelector(({ router }) => router);
  const userId = parseInt(location.pathname.split("/")[2]);
  const user = users.filter((u) => u.id === userId)[0] || {};
  return (
    <Layout>
      <Card>
        <h2>
          {user.firstName} {user.lastName}
        </h2>
        <Info>
          <div>Id: {user.id}</div>
          <div>Phone: {user.phone}</div>
          <div>Email: {user.email}</div>
        </Info>
      </Card>
    </Layout>
  );
};

export default User;
