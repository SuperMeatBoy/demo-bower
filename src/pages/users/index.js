import React from "react";
import { Helmet } from "react-helmet";

import endpoints from "constants/endpoints";
import { useRole } from "hooks/useRole";
import Layout from "components/layout";
import UserTable from "./components/table";
import { Card } from "../../globalStyle";

const Users = () => {
  return (
    <Layout>
      <Helmet title="Users" />
      <Card>
        <UserTable />
      </Card>
    </Layout>
  );
};

export default Users;
