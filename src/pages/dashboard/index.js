import React from "react";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { Card } from "globalStyle";

import endpoints from "constants/endpoints";
import { useRole } from "hooks/useRole";
import Layout from "components/layout";

const Dashboard = () => {
  return (
    <Layout>
      <Helmet title="Dashboard" />
      <Card style={{ minHeight: "300px" }}>
        <FormattedMessage id="dashboard.title" defaultMessage="Dashboard" />
      </Card>
    </Layout>
  );
};

export default Dashboard;
