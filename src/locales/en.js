const messages = {
  "dashboard.title": "Dashboard en-US",
};

export default {
  locale: "en",
  messages,
};
