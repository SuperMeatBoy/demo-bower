import ROLES from "constants/roles";
const { SUPER_ADMIN, ADMIN } = ROLES;
export const menuData = [
  {
    title: "Dashboard",
    key: "dashboards",
    icon: "home",
    url: "/dashboard",
  },
  {
    title: "Admins",
    key: "admins",
    icon: "admin",
    url: "/admins",
    roles: [SUPER_ADMIN],
  },
  {
    title: "Users",
    key: "users",
    icon: "user",
    url: "/users",
    roles: [SUPER_ADMIN, ADMIN],
  },
  {
    title: "Products",
    key: "products",
    icon: "product",
    url: "/products",
    roles: [SUPER_ADMIN, ADMIN],
  },
  {
    title: "Stations",
    key: "stations",
    icon: "station",
    url: "/stations",
  },
  {
    title: "Support",
    key: "support",
    icon: "email",
    url: "/support",
  },
];
