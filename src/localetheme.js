import React from "react";
import { IntlProvider } from "react-intl";
import { useSelector } from "react-redux";

import english from "./locales/en";
import swedish from "./locales/sv";
import { ThemeProvider } from "styled-components";

const locales = {
  en: english,
  sv: swedish,
};

const LocaleTheme = ({ children }) => {
  const { locale, theme } = useSelector(({ root }) => root);
  const currentLocale = locales[locale];
  return (
    <ThemeProvider theme={theme}>
      <IntlProvider
        locale={currentLocale.locale}
        messages={currentLocale.messages}
      >
        {children}
      </IntlProvider>
    </ThemeProvider>
  );
};

export default LocaleTheme;
