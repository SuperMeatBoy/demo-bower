import React, { lazy, Suspense } from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import { ConnectedRouter } from "connected-react-router";

const routes = [
  // Auth Pages
  {
    path: "/auth/login",
    Component: lazy(() => import("pages/auth/login")),
    exact: true,
  },
  {
    path: "/auth/forgot-password",
    Component: lazy(() => import("pages/auth/forgot-password")),
    exact: true,
  },
  // Dashboards
  {
    path: "/dashboard",
    Component: lazy(() => import("pages/dashboard")),
    exact: true,
  },
  // Admins
  {
    path: "/admins",
    Component: lazy(() => import("pages/admins")),
    exact: false,
  },
  // Users
  {
    path: "/users",
    Component: lazy(() => import("pages/users")),
    exact: false,
  },
  {
    path: "/user/:id",
    Component: lazy(() => import("pages/users/components/user")),
    exact: true,
  },
  // Products
  {
    path: "/products",
    Component: lazy(() => import("pages/products")),
    exact: false,
  },
  // Stations
  {
    path: "/stations",
    Component: lazy(() => import("pages/stations")),
    exact: true,
  },
  // Support
  {
    path: "/support",
    Component: lazy(() => import("pages/support")),
    exact: true,
  },
];

const Router = ({ history }) => {
  return (
    <ConnectedRouter history={history}>
      <Route
        render={(state) => {
          const { location } = state;
          return (
            <Switch location={location}>
              <Route
                exact
                path="/"
                render={() => <Redirect to="/dashboard" />}
              />
              {routes.map(({ path, Component, exact, ...rest }) => (
                <Route
                  path={path}
                  key={path}
                  exact={exact}
                  render={() => {
                    return (
                      <Suspense fallback={null}>
                        <Component {...rest} />
                      </Suspense>
                    );
                  }}
                />
              ))}
              <Redirect to="/auth/404" />
            </Switch>
          );
        }}
      />
    </ConnectedRouter>
  );
};

export default Router;
